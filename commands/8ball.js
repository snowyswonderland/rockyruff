exports.run = async (client, message, params) => {
  const Discord = require('discord.js'),
        question = params.slice(0).join(' '),
        fortunes = [
          "Definitely",
          "Yes",
          "Maybe",
          "No",
          "Definitely Not",
          "I'd say no",
          "Answer is looking quite good",
          "Reply hazy"
          ],
        embed = new Discord.MessageEmbed()
  .setDescription(fortunes[Math.floor(Math.random() * fortunes.length)])
  .setColor(message.guild.member(client.user).displayHexColor)
  .setFooter(message.author.username + " asked: " + question, message.author.avatarURL)
  .setTimestamp()
    if (!question) return message.channel.send("VIOLATION CODE 404\n```NO QUESTION PROVIDED```")
  message.channel.send(embed)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "8ball",
  description: "Gives you a fortune (or just answers a question)",
  usage: "!8ball [question]"
}
