 exports.run = async (client, message) => {
  const config = require('../config.json')
  if (message.author.id !== config.ownerID) return
  message.channel.send("Do you want to escape?");

  const validAnswers = ["yes", "y", "no", "n", "cancel"];
  const collector = message.channel.createMessageCollector(m=>m.author.id === message.author.id, {time:30000});

  collector.on("collect", async m => {
    const lower = m.content.toLowerCase();
    if (lower === "cancel" || lower === "no" || lower === "n") {
      return collector.stop("abort");
    } else if (lower === "yes" || lower === "y") {
      return collector.stop("kill");
    }
    return message.channel.send(`Only \`${validAnswers.join("`, `")}\` are valid, please supply one of those.`);
  });

  collector.on("end", async (collected, reason) => {
    if (reason === "kill") {
      await message.channel.send("Waiting for the torches...");
      await client.destroy();
      process.exit();
    } else if (reason === "time") {
      return message.channel.send("VIOLATION CODE 101\n```TIMED OUT```");
    } else if (reason === "abort") {
      return message.channel.send("ABORTED");
    }
  });

};

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
};

exports.help = {
  name: "reboot",
  description: "This reboots the bot.",
  usage: "!reboot"
};
