
exports.run = async (client, message, params) => {
const mock = require('../functions/mock.js'),
      messagea = params.slice(0).join(' ')

 message.channel.send(mock(messagea))
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "mock",
  description: "Mocks a message of your choice!",
  usage: "!mock [message]"
}
