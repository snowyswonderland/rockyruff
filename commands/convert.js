var convert = require('convert-units');
var units = convert().possibilities()
var Discord = require('discord.js');
exports.run = (client, message, [number, unit1, unit2]) => {
            if (units.indexOf(unit1) != -1) {
                if (units.indexOf(unit2) != -1) {
                    if (convert().from(unit1).possibilities().indexOf(unit2) != -1) {
                        let result = Math.round(convert(Number(number)).from(unit1).to(unit2) * 100) / 100;
                        message.channel.send('**Unit Conversion**', {
                            embed: {
                              color: "RANDOM",
                                fields: [{
                                        name: 'Input',
                                        value: number + ' ' + (number == 1 ? convert().describe(unit1).singular : convert().describe(unit1).plural)
                                    },
                                    {
                                        name: 'Output',
                                        value: result + ' ' + (result == 1 ? convert().describe(unit2).singular : convert().describe(unit2).plural)
                                    }
                                ]
                            }
                        });
                    } else {
                        message.channel.send('Could not convert **' + convert().describe(unit1).plural + '** to **' + convert().describe(unit2).plural + '**!');
                    }
                } else {
                    message.channel.send('Unit 2 is invalid. For a list of supported units, see here: https://github.com/ben-ng/convert-units#supported-units');
                }
            } else {
                message.channel.send('Unit 1 is invalid. For a list of supported units, see here: https://github.com/ben-ng/convert-units#supported-units')
}
    }

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
};

exports.help = {
  name: "convert",
  description: "Converts between two units.",
  usage: "!convert [number] [unit 1] [unit 2]"
};
