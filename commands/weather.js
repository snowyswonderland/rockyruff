const weather = require("weather-js")
const Discord = require("discord.js")
exports.run = (client, message, params) => {
  weather.find({search: params.join(" "), degreeType: 'F'}, function(err, result) {
            if (err) message.channel.send(err);
            if (result.length === 0) {
                message.channel.send('**Please enter a valid location.**')
                return;
            }

            var current = result[0].current;
            var location = result[0].location;
console.log(result[0])
            const embed = new Discord.MessageEmbed()
              .setAuthor("Requested by " + message.author.username, message.author.avatarURL)
                .setDescription(`**${current.skytext}**`)
                .setTitle(`Weather for ${current.observationpoint}`)
                .setThumbnail(current.imageUrl)
                .setColor("RANDOM") 
                .addField('Timezone',`UTC${location.timezone}`, true) // This is the first field, it shows the timezone, and the true means `inline`, you can read more about this on the official discord.js documentation
                .addField('Degree Type',location.degreetype, true)// This is the field that shows the degree type, and is inline
                .addField('Temperature',`${current.temperature} Degrees`, true)
                .addField('Feels Like', `${current.feelslike} Degrees`, true)
                .addField('Winds',current.winddisplay, true)
                .addField('Humidity', `${current.humidity}%`, true)
                .setFooter(current.date)
                
                // Now, let's display it when called
                message.channel.send({embed});
        });
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
};

exports.help = {
  name: "weather",
  description: "Displays weather for a given location",
  usage: "!weather [location]"
};
