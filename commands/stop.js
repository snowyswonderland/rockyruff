exports.run = async (client, message, params) => {
  let queue = client.queue.get(message.guild.id);client
  if (!queue) return
  queue.queue = [];
    queue.connection.dispatcher.end();
  message.channel.send("stopped the queue")
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "stop",
  description: "stops le queue",
  usage: "!stop"
}
