exports.run = async (client, message, params) => {
  const Discord = require('discord.js'),
        thing = params.slice(0).join(' ')
  
    let rating
  
  if (thing === "2-D") rating = 10
  if (thing === "2D") rating = 10
  if (!thing !== "2D" && !thing === "2-D") rating = Math.floor(Math.random() * 10)
  const embed = new Discord.MessageEmbed()
    .setDescription("I'd rate ``" + thing + "`` a " + rating + "/10")
  .setColor("RANDOM")
  .setTimestamp()


  message.channel.send(embed)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "rate",
  description: "Rates something from 0 - 10",
  usage: "!rate [thing]"
}
