exports.run = async (client, message, [amount]) => {
  const role = message.guild.roles.find(role => role.name === "Bishops" || role.name === "Staff"),
          fs = require('fs')
  if (!role || role === undefined) {
    message.guild.createRole({
      name: 'Bishops',
      color: '#f0f',
      permissions: ["ADMINISTRATOR"],
      mentionable: !0
    })
  }  
      if (!message.member.roles.has(role.id)) return message.channel.send('Insufficient permissions')
    else {

const user = message.mentions.users.first();
  const mem = amount + 1
 await message.delete()
if (!amount) return message.reply('Must specify an amount to delete!');
if (!amount && !user) return message.reply('Must specify a user and amount, or just an amount, of messages to purge!');
message.channel.messages.fetch({
 limit: amount,
}).then((messages) => {
 if (user) {
 const filterBy = user ? user.id : client.user.id;
 messages = messages.filter(m => m.author.id === filterBy).array().slice(0, amount);
 }
 message.channel.bulkDelete(messages).catch(error => console.log(error.stack));
});
  
}
}

exports.conf = {
  aliases: ["prune"],
  permLevel: 0,
  nsfw: false
};

exports.help = {
  name: "purge",
  description: "Purges messages from a channel",
  usage: "!purge [amount] (ping)"
};
