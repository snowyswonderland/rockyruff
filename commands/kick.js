exports.run = async (client, message, params) => {
var modRole = message.guild.roles.find(r => r.name === "Staff");
            var reason = params.slice(1).join(" ");
            if (!message.member.roles.has(modRole.id)) return;
                 let kickMember = message.guild.member(message.mentions.users.first());
            if (!kickMember) return message.channel.send("You must mention someone to kick!");
                  message.guild.member(kickMember).kick(reason);
              message.channel.send(`${kickMember.user.username} has been kicked!\n Reason: ${reason}`);
      
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "kick",
  description: "Kicks a user",
  usage: "!kick [user] (reason)"
}
