exports.run = async (client, message, params) => {
  const fm = require('lastfm-api-fixed'),
      fmapi = new fm({
        "api_key": process.env.LASTFMKEY,
        "secret": process.env.LASTFMSECRET
      }),
      { MessageEmbed } = require('discord.js'),
        artist = params.slice(0).join(' ')
  
  fmapi.artist.getInfo({artist: artist}, (err, a) => {
   if (err) return message.channel.send('Artist not found...?')
    if (a) {
     const embed = new MessageEmbed()
     .setAuthor('Requested by ' + message.author.username, message.author.avatarURL)
     .setTitle(a.name)
     .setDescription(a.bio.summary.split(' <a')[0] + `... [Learn more on last.fm](${a.url})`)
     .setColor('RANDOM')
     .setThumbnail(a.image[3]['#text'])
     .setFooter('Similar Artists: ' + a.similar.artist.map(a => a.name).join(", "))
     message.channel.send(embed)
    }
  })
}

exports.conf = {
  aliases: ['artist'],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "artistinfo",
  description: "Gets info on an artist/band",
  usage: "!artistinfo [artist]"
}

