exports.run = async (client, message, params) => {
  client.tags.delete(params[0]) 
    message.reply('has deleted tag ' + params[0])
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "deletetag",
  description: "Deletes a tag",
  usage: "!deletetag [name]"
}

