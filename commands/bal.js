exports.run = async (client, message, params) => {
const user = message.mentions.users.first() || message.author
client.credits.ensure(user.id, { credits: 0, gold: 0, daily: false,
    time: null, used: null})
const { MessageEmbed } = require('discord.js'),
      embed = new MessageEmbed()
.setAuthor(user.username, user.avatarURL)
.setColor('RANDOM')
.setDescription("User has " + client.credits.getProp(user.id, "credits") + " credits.")

message.channel.send(embed)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "bal",
  description: "Shows you how many credits you have.",
  usage: "!bal (@user)"
}
