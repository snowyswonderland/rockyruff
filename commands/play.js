exports.run = async (client, message, params) => {
  const play = require('../functions/play.js'),
        handleVideo = require("../functions/handleVideo.js"),
        yt = require('simple-youtube-api'),
        youtube = new yt(process.env.YOUTUBEAPIKEY)
  let VC = message.member.voice.channel
    if (!VC) return

    let url = params[0] ? params[0].replace(/<(.+)>/g, '$1') : '';
    let pl = /^https?:\/\/(www.youtube.com|youtube.com)\/playlist(.*)$/

    let searchString = params.join(' ');
    if (!url || !searchString) return message.channel.send("Provide a URL to a playlist or video OR provide a search term.")

    let perms = VC.permissionsFor(message.client.user);
    if (!perms.has('CONNECT')) return message.channel.send("I can't join the voice channel...")
    if (!perms.has('SPEAK')) return message.channel.send("I can't play music in the voice channel...")

    if (url.match(pl)) {
        let playlist = await youtube.getPlaylist(url);
        let videos = await playlist.getVideos();

        for (const vid of Object.values(videos)) {
            let video = await youtube.getVideoByID(vid.id)
            await handleVideo(client, video, message, VC, true)
        }

        return message.channel.send(`🎵 **${playlist.title}** has been added to queue.`);
    } else {

        try {
            var video = await youtube.getVideo(url);
        } catch (err) {
            if (err) undefined;
            try {
                var vid = await youtube.searchVideos(searchString, 1);
                var video = await youtube.getVideoByID(vid[0].id);
            } catch (err) {
                console.error(err);
                return
            }
        }
        return handleVideo(client, video, message, VC, false);
    }}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "play",
  description: "does a play of music",
  usage: "!play [Video URL] or [Playlist URL] or [Search Terms]"
}

