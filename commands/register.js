exports.run = async (client, message, params) => {


  
  const role = message.guild.roles.find(r => r.name === 'Staff')
  if (!message.guild.member(message.author).roles.has(role.id)) return
  
  const user = message.mentions.users.first()
  if (!user) return message.reply('you\'ve gotta ping someone to let me know they stole an ingie :triumph:')
  client.ingots.ensure(user.id, {
    confirmed: 0
  })
  client.credits.ensure(user.id, {
    credits: 0,
    gold: 0,
    daily: false,
    time: null,
    used: null
  })
  
  const points = params[1] ? parseInt(params[1]) : 1
  
  client.ingots.math(user.id, '+', points, "confirmed")
message.channel.send(`Looks like <@${user.id}> got this :pensivebread:\n\n\`\`\`User's current confirmed hits: ${client.ingots.getProp(user.id, 'confirmed')}\`\`\``)
  function isEven(num) {
    return num % 2 === 0;
}
  if (isEven(client.ingots.getProp(user.id, "confirmed"))) {
    const roles = {
      2: "Bronze II",
      4: "Bronze III",
      6: "Silver I",
      8: "Silver II",
      10: "Silver III",
      12: "Gold I",
      14: "Gold II",
      16: "Gold III"
    }
    
    if (message.guild.roles.find(role => role.name === roles[client.ingots.getProp(user.id, "confirmed")])) {
              message.guild.member(user).roles.add(message.guild.roles.find(role => role.name === roles[client.ingots.getProp(user.id, "confirmed")]).id)
      if (!message.guild.member(user).roles.has(message.guild.roles.find(role => role.name ===  'Silver').id) && message.guild.member(user).roles.has(message.guild.roles.find(role => role.name ===  'Silver I').id)) return message.guild.member(user).roles.add(message.guild.roles.find(role => role.name ===  'Silver').id)
if (!message.guild.member(user).roles.has(message.guild.roles.find(role => role.name ===  'Gold').id) && message.guild.member(user).roles.has(message.guild.roles.find(role => role.name ===  'Gold I').id)) return message.guild.member(user).roles.add(message.guild.roles.find(role => role.name === 'Gold').id)
      message.channel.send(`<@${user.id}> has ranked up to ${roles[client.ingots.getProp(user.id, "confirmed")]}!`)
        }
  }
}

exports.conf = {
  aliases: [],
  permLevel: 0
}

exports.help = {
  name: "register",
  description: "registers a steal",
  usage: "!register [user] (total)"
}