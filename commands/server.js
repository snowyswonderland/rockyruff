exports.run = async (client, message, params) => {
  const Discord = require("discord.js"),
        serverData = message.guild
       var embed = new Discord.MessageEmbed()
                .setTitle(`Info for ${serverData.name}`)
                .setThumbnail(serverData.iconURL)
                .setDescription(`Server since ${serverData.createdAt}\nMember Count: ${serverData.members.size}\nRegion: ${serverData.region}\nOwner: ${serverData.owner}\nEmotes: ${message.guild.emojis.map(m=> `${m.name}`).join(", ")}`)
       .setColor("RANDOM")
            
            message.channel.send(embed);
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "server",
  description: "Shows info about the server",
  usage: "!server"
}

