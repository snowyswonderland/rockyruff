exports.run = async (client, message, params) => {
const { Canvas } = require("canvas-constructor"); // You can't make images without this.
const { resolve, join } = require("path"); // This is to get a font file.
const { MessageAttachment } = require("discord.js"); // This is to send the image via discord.
const text = params.slice(0).join(' ')
var assets = require("../assets");
var express = require("express");

var app = express();
app.use("/assets", assets);


  function trenchify() {
      Canvas.registerFont(resolve(join(__dirname, "/EurostileBQ.otf")), "Eurostile");

return new Canvas(1000, 180)
    .setStroke("#FCE300")
  .setTextAlign("center")
  .setTextFont("85pt Eurostile")
        .measureText(text, function(size) {
        const newSize = size.width < 1000 ? 85 : (1000 / size.width) * 85;
        this.setTextFont(`${newSize}pt Eurostile`);
    })
    .addStrokeText(text, 500, 106)
    .addStrokeText(text, 500, 98)
   
    
.toBuffer()
}


      await message.channel.send(new MessageAttachment(await trenchify(), `trenchified.jpg`));

  
  

}

exports.conf = {
  aliases: ["ttxt"],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "trenchtxt",
  description: "Trenchifies your text",
  usage: "!trenchtxt [text]"
}
