exports.run = (client, message, params) => {
  if (message.author.id !== '447548069030920202') return
  let command;
  if (client.commands.has(params[0])) {
    command = params[0];
  } else if (client.aliases.has(params[0])) {
    command = client.aliases.get(params[0]);
  }
  if (!command) {
    return message.channel.send(`I cannot find the command: ${params[0]}`);
  } else {
    message.channel.send(`Reloading: ${command}`)
    .then(m => {
      client.reload(command)
      .then(() => {
        m.edit(`Successfully reloaded: ${command}`);
      })
      .catch(e => {
        m.edit(`Command reload failed: ${command}\n\`\`\`${e.stack}\`\`\``);
      });
    });
  }
};

exports.conf = {
  aliases: ["r"],
  permLevel: 0,
  nsfw: false
};

exports.help = {
  name: "reload",
  description: "Reloads the command file, if it's been updated or modified.",
  usage: "reload <commandname>"
};
