exports.run = async (client, message, params) => {
  const thing = params.slice(0).join(' '),
        Discord = require('discord.js'),
        lyric = require('lyricist'),
        lyr = new lyric('exzKax1DFpxgTVQsy9eJfMH75eBFTpgSv4e__BAzrTLp5nSgxfDiAOBjSbZw2tLc'),        
        mx = require('node-hackgenius'),
        ytapi = require("simple-youtube-api"),
        youtube = new ytapi(process.env.YOUTUBEAPIKEY)
  let song
  if (!params) return message.channel.send("i need a search term...")
  if (params) song = thing
  let id
  let url
  let artistic
  let sang

    const results = await youtube.searchVideos(song, 1);
  mx.search(song).then(async songs => {
    console.log(songs)
    id = songs[0].id
    url = songs[0].url
    artistic = songs[0].artist
    sang = songs[0].title
  
  const songz = await lyr.song(id, {fetchLyrics: true})
  console.log(songz.lyrics)
  const embed = new Discord.MessageEmbed()
  .setTitle('Lyrics for ' + sang + " by " + artistic)
  .setTimestamp()
  .setURL(url)
  .setColor("RANDOM")
  .setDescription(songz.lyrics.substring(0, 2048))

  const embed2 = new Discord.MessageEmbed()
  .setDescription(songz.lyrics.substring(2049, 4094))
  .setColor("RANDOM")
  
    const embed3 = new Discord.MessageEmbed()
  .setDescription(songz.lyrics.substring(4095, 6140))
  .setColor("RANDOM")
    
  message.channel.send(embed)
  if (songz.lyrics.length > 2045) {
   message.channel.send(embed2)
    if (songz.lyrics.length > 4091) {
     message.channel.send(embed3) 
    }
  }
  })
}
                       
exports.conf = {
  aliases: ['l'],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "lyrics",
  description: "Gets lyrics to a song you choose",
  usage: "!lyrics [song title]"
}
