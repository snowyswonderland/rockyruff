exports.run = async (client, message, params) => {
  const Discord = require('discord.js'),
        moment = require('moment')
  require('moment-duration-format')
  const Trench = client.user.id
  var onlineSince = moment.duration(client.uptime).format(" D [days], H [hrs], m [mins], s [secs]")
 var channels = client.channels.size
 var users = client.users.size 
 var username = client.user.username
 var avatar = client.user.avatarURL
 var nickname = message.guild.member(client.user).displayName
 var color = message.guild.member(client.user).displayHexColor
const bots = client.users.filter(user => user.bot).size
const user = client.users.filter(user => !user.bot).size
    let msgping1 = new Date();

    let clientping = new Date() - message.createdAt;

    let msgping2 = new Date() - msgping1;

    const embed = new Discord.MessageEmbed()
  .setTitle("2-Dingot")
  .setColor("RANDOM")
  .setDescription('Info and Stats')
  .addField("Online For", `${onlineSince}`, true)
   .addField("Channels", channels, true)
   .addField("Users", user + ' users, ' + bots + ' bots, ' + users + ' total', true)
   .setThumbnail(avatar)
  .addField('Creator', '<@447548069030920202>')
    .addField("Commands", client.commands.size, true)
  .addField('API Ping : ', Math.floor(client.ping) + 'ms')
        .addField('Bot Ping : ', Math.floor(clientping) + 'ms')
        .addField('Message Ping : ', '~' + Math.round(msgping2) + 'ms')
  message.channel.send(embed)
  

}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "info",
  description: "Shows info about Trench Bot",
  usage: "!info"
}
