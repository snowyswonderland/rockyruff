exports.run = async (client, message, params) => {
  let queue = client.queue.get(message.guild.id);
    if (!queue) return
    
    if (!params[0]) return
    if (isNaN(params[0])) return 
    if (params[0] < 0 || params[0] > 100) return

    queue.volume = params[0];
    queue.connection.dispatcher.setVolumeLogarithmic(params[0] / 100);

    return message.channel.send(`🎵 Volume has now been set to **${queue.volume}/100**`);
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "volume",
  description: "changes the volume",
  usage: "!volume [1-100]"
}
