exports.run = async (client, message, params) => {
const roleColor = require('../functions/roleColor.js'),
      guild = message.guild,
      role = params.slice(0).join(' '),
      rolestuff = message.guild.roles.find(r => r.name === role),
      { MessageEmbed } = require('discord.js'),
      moment = require("moment"),
      embed = new MessageEmbed()
require('moment-duration-format')
if (!rolestuff) message.channel.send("role not found")
  embed.setTitle(rolestuff.name)
  embed.setColor("RANDOM")
  embed.setDescription(`**ID** - ${rolestuff.id}\n**Created** - ${moment(rolestuff.createdAt).format('DD MM YYYY')}\n**Permissions** - ${rolestuff.permissions.toArray()}\n**Mentionable?** - ${rolestuff.mentionable}\n**Hex Color** - ${rolestuff.hexColor}\n**Users** - ${rolestuff.members.size}`)
  
  message.channel.send(embed)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "role",
  description: "shows info about a role",
  usage: "!role [name]"
}
