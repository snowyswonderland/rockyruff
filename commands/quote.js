exports.run = async (client, message, params) => {

    const name = params[0];
    if(!client.quotes.has(name)) return message.channel.send(`The quote \`${name}\` does not exist.`) 
       const quote = client.quotes.get(name);
   const embed = client.quotes.getProp(name, "embed")
    await message.channel.send({embed: embed});
   message.delete()
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
};

exports.help = {
  name: "quote",
  description: "Quotes a users message",
  usage: "!quote [add, delete, list, a quote name] [options]"
};
