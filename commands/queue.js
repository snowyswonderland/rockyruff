exports.run = async (client, message, params) => {
  const { MessageEmbed } = require('discord.js')
      let queue = client.queue.get(message.guild.id);
    if (!queue) return

    let embed = new MessageEmbed()
        .setColor('RANDOM')
        .setThumbnail(client.user.avatarURL)
        .setDescription(`**-=- Music Queue -=-**\n${queue.queue.map(music => 
            `**-** ${music.title}`).join('\n')}\n\n🎵 **Currently Playing:** ${queue.queue[0].title}`);

    message.channel.send(embed);
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "queue",
  description: " shows le queue",
  usage: "!queue"
}
