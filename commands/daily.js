exports.run = async (client, message, params) => {
client.credits.ensure(message.author.id, {
  credits: 0,
  gold: 0,
  daily: false,
  time: null,
  used: null
})
  
  if (client.credits.getProp(message.author.id, "daily") === true) return message.channel.send("You already got dailies.")
    client.credits.math(message.author.id, '+', 350, 'credits')
    message.channel.send("boom check your bal")
  client.credits.setProp(message.author.id, "daily", true)
  client.credits.setProp(message.author.id, "time", 60*60*24*1000)
    client.credits.setProp(message.author.id, "used", new Date().getTime())
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "daily",
  description: "gives ya 350 credits",
  usage: "!daily"
}
