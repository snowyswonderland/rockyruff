
exports.run = async (client, message, params) => {
const vc = message.member.voice.channel
await vc.leave()
  client.radio.delete(message.guild.id)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "leave",
  description: "Leaves vc",
  usage: "!leave"
}
