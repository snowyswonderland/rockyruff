exports.run = async (client, message, params) => {
  client.score.ensure(message.author.id, {
    points: 0,
    level: 1,
    levelUp: "You have no life and have leveled up to level {level}."
  })
  
const oldLUM = client.score.getProp(message.author.id, "levelUp"),
      newLUM = params.slice(0).join(' ')

if (newLUM) {
message.channel.send('Set your level up message to ```' + newLUM + '```')
  client.score.setProp(message.author.id, "levelUp", newLUM)
} else return message.channel.send("your current level up message is\n\n```" + oldLUM + "```")
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "levelup",
  description: "Sets a custom level-up message",
  usage: "!levelup [message]"
}
