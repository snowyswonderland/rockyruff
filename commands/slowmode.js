exports.run = async (client, message, params) => {
const time = parseInt(params[0])
const valid = [
  '406643416789942298',
  '447548069030920202'
  ]
  if (!valid.includes(message.author.id)) return
  message.channel.setRateLimitPerUser(time)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "slowmode",
  description: "sets slowmode",
  usage: "!slowmode [time]"
}
