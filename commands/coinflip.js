exports.run = async (client, message, params) => {
const Discord = require('discord.js'),
      coins = [ "heads", "tails" ],
      embed = new Discord.MessageEmbed()
.setTitle('You flipped a coin!')
.setColor('RANDOM')
.setDescription('It landed on ' + coins[Math.floor(Math.random() * coins.length)] + '!')

message.channel.send(embed)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "coinflip",
  description: "Flips a coin",
  usage: "!coinflip"
}
