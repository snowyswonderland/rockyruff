exports.run = async (client, message, params) => {
    const name = params[0],
          { MessageEmbed } = require('discord.js'),
        content = params.slice(1).join(' ') ? params.slice(1).join(' ') : ""
 client.tags.set(name, {
   content: content, 
   attachments: ""
 })
  if (message.attachments.size > 0) client.tags.setProp(name, "attachments", message.attachments.first().url)
       const embed = new MessageEmbed()
  .setTitle(name)
  .setColor("RANDOM")
  .setDescription(`\`\`\`${content}\`\`\``)
       
       if (message.attachments.size > 0) {
         client.tags.setProp(name, "attachments", message.attachments.first().url)
         embed.setImage(message.attachments.first().url)
       }
  
  message.channel.send('Tag ' + name + ' has been successfully created', embed)
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "createtag",
  description: "Creates a tag",
  usage: "!createtag [name] [stuff]"
}

