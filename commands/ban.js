exports.run = async (client, message, params) => {
var modRoless = message.guild.roles.find(r => r.name === "Staff");
            if (!message.member.roles.has(modRoless.id)) return;
            var banMember = message.guild.member(message.mentions.users.first());
            if (!banMember) return message.channel.send("You must mention a member to ban!");
            var reason = params.slice(1).join(" ");
      message.guild.member(banMember).ban(reason);
      message.channel.send(`${banMember.user.username} has been banned.\n Reason: ${reason}`);

}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "ban",
  description: "bans a user",
  usage: "!ban [user] (reason)"
}
