exports.run = async (client, message, params) => {
  const ihr = require("iheartradio");
ihr.search(params.slice(0).join(' '), {
  "secure": false,
  "maxRows": 10, 
  "bundle": false,
  "station": true,
  "artist": false,
  "album": false,
  "track": false,
  "playlist": false,
  "podcast": false
  }).then(async (results) => {
  let index = 0
  let number
  message.channel.send(results.stations.map(m => `${++index} - ${m.name}`).join('\n')).then(e=> {
    e.channel.awaitMessages(msg2 => parseInt(msg2.content) > 0 && parseInt(msg2.content) < 11, {
							max: 1,
							time: 10000,
							errors: ['time']
						}).then(v => {
      number = v.first().content
    message.channel.send('Now streaming: ``' + results.stations[number - 1].name + '``')
  ihr.getStreamInfo(results.stations[number - 1].id).then(async (stream) => {
console.log(stream)
    let VC = message.member.voice.channel
    if (!VC) return
    let perms = VC.permissionsFor(message.client.user);
    if (!perms.has('CONNECT')) return
    if (!perms.has('SPEAK')) return 
    const { get } = require('snekfetch')
    const  str = stream.streams.hls_stream
    if (!client.radio.get(message.guild.id)) {
        client.radio.set(message.guild.id, null)
        }
    await VC.join().then(connection => {
    let dispatcher = connection.play(str)
                .on('end', () => {
                }).on('error', err => console.error(err));
    })
    })
  })
}).catch(console.error);
}).catch(console.error);
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "radio",
  description: "Streams a radio station!",
  usage: "!radio [radio name]"
}

