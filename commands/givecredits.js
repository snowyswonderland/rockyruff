exports.run = async (client, message, params) => {
const valid = [
  "Staff"
  ]

  if (!valid.includes(message.guild.roles.find(role => role.name === 'Staff').name)) return

const added = parseInt(params[1])
  if (isNaN(added)) return message.reply("provide a number you dummy")  
  const user = message.mentions.users.first()
  
  client.credits.ensure(user.id, {
    credits: 0,
    gold: 0,
    daily: false,
    time: null,
    used: null
  })
  
  client.credits.math(user.id, '+', added, "credits")
  
  message.channel.send(user.username + " now has " + client.credits.getProp(user.id, "credits") + " credits.")
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "givecredits",
  description: "gives a user credits",
  usage: "!givecredits [user] [credits]"
}
