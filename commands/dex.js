String.prototype.toProperCase = function(opt_lowerCaseTheRest) {
  return (opt_lowerCaseTheRest ? this.toLowerCase() : this)
    .replace(/(^|[\s\xA0])[^\s\xA0]/g, function(s){ return s.toUpperCase(); });
};
exports.run = async (client, message, params) => {
 const Discord = require('discord.js')
  const oakdexPokedex = require('oakdex-pokedex')
  const Pokedex = require('pokedex-promise-v2')
  const getLatestGen = require('../functions/getLatestGen.js')
  var P = new Pokedex();

                 const embedcolors = {
  Red: "0x8B0000",
  Blue: "0x0000FF",
  Yellow: "0xFFFF00",
  Green: "0x008000",
  Black: "0x000000",
  Brown: "0xA52A2A",
  Purple: "0x800080",
  Gray: "0x808080",
  White: "0xFFFFFF",
  Pink: "0xFFC0CB"
}
                   const pokemon = params.join(" ").split(' --')[0].toProperCase()
                   const poke = params.join(' ').split(' --')[0]

                    P.getPokemonSpeciesByName(poke)
    .then(function(response) {
    })

oakdexPokedex.findPokemon(pokemon, function(p) {
      if(!p) return message.channel.send('Could not find the Pokemon ``' + poke + '``')
var entry = getLatestGen(p.pokedex_entries)

const PokemonGIF = params.join('').includes('--') ? params.join('').split('--')[0] : params.join('')
console.log(PokemonGIF)
  let options = params.join(' ').includes(' --') ? params.join(' ').split(' --')[1] : ''
let stuff = {
 'shiny': 'xyani-shiny',
  '': 'xyani'
}
console.log(stuff[options])
var ab = p.abilities.map(a => a.name).join(', ')
var gender = p.gender_ratios === null ?  `No Genders` : `${p.gender_ratios.male}% Male\n${p.gender_ratios.female}% Female` 

   var embed = new Discord.MessageEmbed()
      .setTitle(`${p.names.en}, the ${p.categories.en}`)
     .setFooter('Types - ' + p.types.join(', ') + '\nAbilities - ' + ab)
     .setColor(embedcolors[p.color])
              .setImage(`https://play.pokemonshowdown.com/sprites/${stuff[options]}/${PokemonGIF.toLowerCase()}.gif`)
     .setDescription(`\`\`\`Height: ${p.height_us}\nWeight: ${p.weight_us}\nBase Stats: ${p.base_stats.hp}, ${p.base_stats.atk}, ${p.base_stats.def}, ${p.base_stats.sp_atk}, ${p.base_stats.sp_def}, ${p.base_stats.speed}\nEV Yields: ${p.ev_yield.hp}, ${p.ev_yield.atk}, ${p.ev_yield.def}, ${p.ev_yield.sp_atk}, ${p.ev_yield.sp_def}, ${p.ev_yield.speed}\nBase EXP Yield: ${p.base_exp_yield}\nEgg Groups: ${p.egg_groups}\nCatch Rate: ${p.catch_rate}\nHatch Time: ${p.hatch_time.slice(',')[0] + " to " + p.hatch_time.slice(',')[1]} Steps\nGender Ratios: ` + gender + `\nEvolves From: ${p.evolution_from ? p.evolution_from : "Doesn't Evolve From Anything"}\nEvolution(s): ${p.evolutions ? p.evolutions.map(e => ` ${e.to}` + (e.item ? " with " + e.item + (e.conditions ? " with conditions: " + e.conditions.join(', ') : "") : (e.level ? " at level " + e.level + (e.conditions ? " with conditions: " + e.conditions.join(", ") : "") : (e.level_up && e.conditions ? " upon level up with conditions: " + e.conditions.join(", ") : (e.happiness ? " at max happiness" + (e.conditions ? " with conditions: " + e.conditions.join(", ") : "") : (e.trade ? " when traded" + (e.hold_item ? " while holding " + e.hold_item : "") : (e.hold_item ? " while holding " + e.hold_item + (e.conditions ? " with conditions: " + e.conditions.join(", ") : "") : (e.move_learned ? " when " + e.move_learned + " is learned" + (e.conditions ? " with conditions: " + e.conditions.join(', ') : "") : undefined)))))))) : "NO EVOLUTIONS..."}` +`\n\n${entry.en}\`\`\``)
   
   message.channel.send(embed)

})
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "dex",
  description: "Its a Pokedex but on Discord!",
  usage: "!dex [pokemon]"
}
