exports.run = async (client, message, params) => {
const ytapi = require("simple-youtube-api");
const youtube = new ytapi(process.env.YOUTUBEAPIKEY)
const Discord = require('discord.js')

const search = params.join(" ");
  try {
    const results = await youtube.searchVideos(search, 1);
    console.log(results)
    
    results.forEach(i => {
      message.channel.send(`**${i.title} - Uploaded by user ${i.channel.title}** https://www.youtube.com/watch?v=${i.id}`)
    })
  } catch (e) {
    message.reply(e.message);
  }
}

exports.conf = {
  aliases: ["yt"],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "youtube",
  description: "Searches for a video on YouTube",
  usage: "!youtube [title]"
}
