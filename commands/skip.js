exports.run = async (client, message, params) => {
let queue = client.queue.get(message.guild.id);
  if (!message.member.voice.channel) return
  if (!queue) return
  queue.connection.dispatcher.end("skip")
  message.channel.send('skipped')
  
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "skip",
  description: "skips current song",
  usage: "!skip"
}
