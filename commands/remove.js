exports.run = async (client, message, params) => {

  const user = message.mentions.users.first()
    const role = message.guild.roles.find(r => r.name === 'Staff')
  if (!message.guild.member(message.author).roles.has(role.id)) return
if (!user) return message.reply("you've gotta let me know who stole an ingie first 😤")
  
  client.ingots.ensure(user.id, {
    "confirmed": 0
  })
  client.credits.ensure(user.id, {
    "credits": 0,
    gold: 0,
    daily: false,
    time: null
  })
                       
  const points = params[1] ? parseInt(params[1]) : 1
      
      client.ingots.math(user.id, '-', points, "confirmed")
  
  message.channel.send(`Looks like <@${user.id}> didnt get this :pensivebread:\n\n\`\`\`User's current confirmed hits: ${client.ingots.getProp(user.id, 'confirmed')}\`\`\``)
  function isEven(num) {
    return num % 2;
}
  if (isEven(client.ingots.getProp(user.id, "confirmed"))) {
    const roles = {
      2: "Bronze II",
      4: "Bronze III",
      6: "Silver I",
      8: "Silver II",
      10: "Silver III",
      12: "Gold I",
      14: "Gold II",
      16: "Gold III"
    }
    
      if (message.guild.member(user).roles.has(message.guild.roles.find(role => role.name ===  'Gold').id) && message.guild.member(user).roles.has(message.guild.roles.find(role => role.name ===  'Gold I').id)) message.guild.member(user).roles.remove(message.guild.roles.find(role => role.name ===  'Gold').id)
        message.guild.member(user).roles.remove(message.guild.roles.find(role => role.name === roles[client.ingots.getProp(user.id, "confirmed") + 1]).id)
      message.channel.send(`<@${user.id}> has been demoted to ${roles[client.ingots.getProp(user.id, "confirmed") - 1]}!`)
        
  }
    }

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "remove",
  description: "removes steals",
  usage: "!remove @user"
}