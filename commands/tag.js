exports.run = async (client, message, params) => {
    const { MessageEmbed } = require('discord.js')

 if (!client.tags.get(params[0])) return message.channel.send('Tag not found!')
  
  if(client.tags.has(params[0])) {
     const content = client.tags.getProp(params[0], "content")
    const embed = new MessageEmbed()
    .setDescription(content)
    .setColor("RANDOM")
    .setImage(client.tags.getProp(params[0], "attachments"))
    
    
    message.channel.send(embed)
     }
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "tag",
  description: "Sends a tag",
  usage: "!tag [name]"
}
