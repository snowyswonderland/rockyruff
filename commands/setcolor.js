exports.run = async (client, message, params) => {
  client.roles.ensure(message.author.id, {
roles: []
  })
  const name = params[0]
  
  const valid = [
    "Red",
    "Orange",
    "Green",
    "Blue",
    "Purple",
    "Pink"
    ]
  
  if (!valid.includes(name)) return message.channel.send('Role name is either not a valid color role or doesn\'t exist.')
    const role = message.guild.roles.find(r=> r.name === name).id
  if (!client.roles.getProp(message.author.id, "roles").includes(role)) return message.channel.send("You don't own this role.")
  
  message.member.roles.remove(message.guild.roles.filter(r=> valid.includes(name)).id)
  message.member.roles.add(role)
  message.reply("set your color ;)")
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "setcolor",
  description: "sets your color",
  usage: "!setcolor [color role name]"
}
