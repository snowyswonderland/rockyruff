exports.run = async (client, message, params) => {
const valid = [
  '406643416789942298',
  '447548069030920202'
  ]
  if (!valid.includes(message.author.id)) return
  if (params[0] !== "stop") {
  message.channel.updateOverwrite(message.guild.roles.find(role => role.name === "Bronze"), {
    SEND_MESSAGES: false
  })
  message.channel.send('lockdown successful')
  }
  if (params[0] === "stop") {
    message.channel.updateOverwrite(message.guild.roles.find(role => role.name === "Bronze"), {
    SEND_MESSAGES: true
  })
    message.channel.send('stopped lockdown')
  }
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "lockdown",
  description: "sets lockdown for a channel",
  usage: "!lockdown"
}
