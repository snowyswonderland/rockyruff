const Discord = require('discord.js')
const moment = require('moment')
require("moment-duration-format");
exports.run = async(client, message, params) => {
  
  const user = message.mentions.users.first() || message.author;

  client.credits.ensure(user.id, {
    credits:0,
    gold: 0,
    daily: false,
    time: 0,
    used: null
  })
  const userJoin = moment.utc(message.guild.member(user).joinedAt).format("MMMM Do YYYY, HH:mm")
    const userNick = message.guild.member(user).displayName
    const userColor = message.guild.member(user).displayHexColor
    const userRoles = message.guild.member(user).roles.first(5)
    const usersss = userRoles.map(m => m.name.toUpperCase()).slice(1).join('  ')
 
  const bot = {
   true: "Yes",
   false: "No"
  }
  
   const { Canvas } = require('canvas-constructor'),
         { MessageAttachment } = require('discord.js'),
         { get } = require('snekfetch'),
         { resolve, join } = require('path')

   const imageUrlRegex = /\?size=2048$/g
    const status = {


         online: "https://upload.wikimedia.org/wikipedia/commons/1/11/Pan_Green_Circle.png",


         idle: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTFhzJ8yt_mKyV0YO-EOVSh7mcMwscKhwfz-MXK5iWBWK1Wpjtq",


         dnd: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQz3yIyLvPX_nWVyb1-oDX1yy6T4rOLX0Q8Fj7R94t6Xe6XnKJo",


         offline: "https://upload.wikimedia.org/wikipedia/commons/b/b3/Solid_gray.png"


        };
  const type = {
   '0': 'playing ',
    '1': 'watching ',
    '2': 'listening to ',
    '3': 'streaming '
  }
      const{ body : stat } = await get(status[user.presence.status])
      const { body : bg } = await get('https://image.ibb.co/nKOfRV/Untitled.jpg')
      const name = user.username.length > 10 ? user.username.substring(0, 7) + "..." : user.username;
  const game = user.presence.game ? type[user.presence.game.type] + user.presence.game.name : "no game"
   
   
   function userinfocan() {
      Canvas.registerFont(resolve(join(__dirname, "/EurostileBQ.otf")), "Eurostile");
     Canvas.registerFont(resolve(join(__dirname, "/tracks.ttf")), "Compacta")

      return new Canvas(825, 825)
      .addImage(bg, 0, 0)
     .setStroke('#FCE300')
     .setTextFont('43pt Eurostile')
     .setTextAlign('center')
     .addStrokeText(name.toUpperCase(), 195, 230)
     .addStrokeText(name.toUpperCase(), 195, 235)
     .setColor('#FCE300')
     .setTextFont('5pt Compacta')
    .addText('GAME. ' + game.toUpperCase() + ' SERVER JOIN. ' + userJoin.toUpperCase() + ' BOT. ' + bot[user.bot].toUpperCase(), 195, 266)
     .addText('STATUS. ' + user.presence.status.toUpperCase() + ' POINTS ' + client.score.getProp(user.id, "points") + ' LEVEL ' + client.score.getProp(user.id, "level"), 195, 277)
     .addText('ROLES. ' + usersss + " GOLDS: " + client.credits.getProp(user.id, "gold") + " CREDITS: " + client.credits.getProp(user.id, "credits"), 195, 288)
     .addText('COLOR. ' + userColor + ' USERNAME. ' + user.username + ' ID. ' + user.id, 195, 299)
     .toBufferAsync()
   }
                      

    
          await message.channel.send(new MessageAttachment(await userinfocan(), 'userInfo.jpg'))
  

}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
};

exports.help = {
  name: "user",
  description: "Shows info for a user",
  usage: "!user (ping)"
};

// sex