exports.run = async (client, message, params) => {
  let Discord = require('discord.js')
 let reminderTime = params.join(' ').split(' | ')[1].split(' ')[0]
 let uni = params.join(' ').split(' | ')[1].split(' ')[1]
    if (!reminderTime) return message.channel.send("Specify a time for me to remind you! (This time is in minutes)")
  const moment = require('moment')

    let reminder = params.join(' ').split(' | ')[0];

  let unit
  let u
  
  if (['seconds', 'second'].some(seconds => uni.includes(seconds))) {
    unit = 1000
    u = "second(s)"
  }
  
  if (['minute', 'minutes'].some(minutes => uni.includes(minutes))) {
    unit = 60000
    u = "minute(s)"
  }

  if (['hour', 'hours'].some(hour => uni.includes(hour))) {
    unit = 3600000
    u = "hour(s)"
  }
  
  if (['day', 'days'].some(day => uni.includes(day))) {
  unit = 360000 * 24
    u = "day(s)"
  }
    let remindEmbed = new Discord.MessageEmbed()
        .setColor("RANDOM")
        .setAuthor(`${message.author.username}`, message.author.displayAvatarURL)
        .addField("Reminder", `\`\`\`${reminder}\`\`\``)
        .addField("Time", `\`\`\`${reminderTime} ${u}\`\`\``)
        .setTimestamp();

    message.channel.send(remindEmbed);
  const newID = client.reminders.size + 1
  client.reminders.set(newID, {
    reminder: reminder,
    user: message.author.id,
    time: new Date().getTime(),
    remindertime: reminderTime*unit
  })
}

exports.conf = {
  aliases: ["rem"],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "remind",
  description: "Creates a reminder for you",
  usage: "!remind [thing] | [time] [second, seconds, minute, minutes, hour, hours, day, days]"
}
