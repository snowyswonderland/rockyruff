exports.run = async (client, message, params) => {
  const urban = require('urban.js')
  urban(params.slice(0).join(' ')).then(async word => {
    const { MessageEmbed } = require('discord.js'),
          urbandi = new MessageEmbed()
    .setTitle(word.word)
    .setColor("RANDOM")
    .setTimestamp()
    .setAuthor('Searched for by ' + message.author.username, message.author.avatarURL)
    .setFooter('' + word.thumbsUp + '' + word.thumbsDown)
    .setDescription('```Definition: "' + word.definition + '"\n\nExample: "' + word.example + '"```')
    await message.channel.send(urbandi)
  }).catch(message.channel.send('Word not found...'))
}

exports.conf = {
  aliases: [],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "urban",
  description: "Gets an urban dictionary definition",
  usage: "!urban [term]"
}

