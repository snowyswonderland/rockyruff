exports.run = async (client, message, params) => {
const { MessageEmbed } = require('discord.js'),
      term = params.slice(0).join(' '),
      scraper = require('images-scraper'),
        bing = new scraper.Bing(),
        moment = require('moment'),
        log = message => {
  console.log(`[${moment().format("YYYY-MM-DD HH:mm:ss")}] ${message}`);
},
      errembed = new MessageEmbed()
.setTitle('404 UNDER CONSTRUCTION')
.setColor("RANDOM")

  
  let page = 0; 
 
  bing.list({
    keyword: term,
    num: 1000
  }).then(function (res) {
    if (!res) return message.channel.send('No results')
  const embed = new MessageEmbed() 
    .setColor("RANDOM")
    .setFooter(`Page ${page + 1} of 1000`) 
    .setImage(res[page].url)
 
  message.channel.send(embed).then(msg => { 
   
    msg.react('⏪').then( r => { 
      msg.react('⏹').then(v => {
        msg.react('⏩')  
        msg.react('🔢')
      const backwardsFilter = (reaction, user) => reaction.emoji.name === '⏪' && user.id === message.author.id;
      const forwardsFilter = (reaction, user) => reaction.emoji.name === '⏩' && user.id === message.author.id; 
      const stopFilter = (reaction, user) => reaction.emoji.name === '⏹' && user.id === message.author.id;
     const ffilterf = (reaction,user) => reaction.emoji.name === '🔢' && user.id === message.author.id
        
      const backwards = msg.createReactionCollector(backwardsFilter ); 
      const forwards = msg.createReactionCollector(forwardsFilter); 
      const stop = msg.createReactionCollector(stopFilter)
            const choose = msg.createReactionCollector(ffilterf)
      
      backwards.on('collect', r => { 
        if (page === 0) return; 
        page--;
        embed.setImage(res[page].url); 
        embed.setFooter(`Page ${page + 1} of 1000`); 
        msg.edit(embed) 
      }) 
     
      forwards.on('collect', r => { 
        if (page === 999) return; 
        page++; 
        embed.setImage(res[page].url); 
        embed.setFooter(`Page ${page + 1} of 1000`); 
        msg.edit(embed) 
      })
      stop.on('collect', r => {
        msg.delete()
      })
        choose.on('collect', r => {
          message.channel.send("Which page would you like to go to?").then(m => {
            const filter = i => i.author === message.author && !isNaN(parseInt(i.content))
            m.channel.awaitMessages(filter, {max: 1}).then(u => {
              page = parseInt(u.first().content) - 1
              if (page > 1000 || page < 1) return message.channel.send("number must be between 1 and 1000")
              embed.setFooter(`Page ${page + 1} of 1000`)
              embed.setImage(res[page].url)
              msg.edit(embed)
            })
          })
        })
  })
  })
  })
})

          
}

exports.conf = {
  aliases: ['img'],
  permLevel: 0,
  nsfw: false
}

exports.help = {
  name: "image",
  description: "Searches for images!",
  usage: "!image [term]"
}
