Object.prototype.length = function()
{
  var i = 0;
  for ( var p in this ) i++;
  return i;
}
module.exports = (client, reaction, user) => {
  client.credits.ensure(reaction.message.author.id, {
    credits: 0,
    gold: 0,
    daily: false,
    time: null,
    used: null
  })
  client.credits.ensure(user.id, {
    credits: 0,
    gold: 0,
    daily: false,
    time: null,
    used: null
  })
  if (reaction.emoji.name === "✅" && reaction.message.channel.id === "508476833730396160") {
    const hits = reaction.message.embeds[0].fields[2]
    console.log(reaction.message.reactions.size)
    if (parseInt(hits.value) < reaction.message.reactions.length()) return reaction.message.delete()
  }
  if (reaction.emoji.name === "gold") {
    if (user.id === reaction.message.author.id) return;
    if (client.credits.getProp(user.id, 'credits') < 1000) {
      reaction.message.channel.send('Giving gold costs 1000 credits')
    } else {
      user.send('Do you want to give gold? To give gold simply say ``gold``. If not, ignore this.').then(m => {
            const filter = r => r.author === user && r.content === "gold"
            m.channel.awaitMessages(filter, {max:1, time: 60000, errors: ['time']}).then(v => {
              client.credits.math(user.id, '-', 1000, 'credits')
                         client.credits.math(reaction.message.author.id, '+', 1, "gold")
              const { MessageEmbed } = require('discord.js'),
                    embed = new MessageEmbed()
              .setColor("RANDOM")
              .setAuthor(reaction.message.author.username + " has been given gold!", reaction.message.author.avatarURL)
              .setThumbnail(user.avatarURL)
              .addField('Given by:', user.username)
              .addField('Message:', reaction.message.content || "...")
              .setTimestamp()            
            .setFooter(reaction.message.author.username + "'s current golds: " + client.credits.getProp(reaction.message.author.id, "gold"))
                if (reaction.message.attachments.size === 0) {
                                   embed.addField('Attachments:', "No Attachments")
                } else {
                                if (![".jpg", ".png", ".jpeg", ".gif"].some(extension => reaction.message.attachments.first().url.includes(extension))) {
                 embed.addField('Attachments:', reaction.message.attachments.first().url)
                }
              else embed.setImage(reaction.message.attachments.first().url)                   
                }                       
              client.channels.get('508510547537559553').send(embed).then(a => {
                a.react('⬆️')
                a.react('⬇️')
              })
              user.send('You now have ``' + client.credits.getProp(user.id, 'credits') + '`` credits')
              if (!reaction.message.author.bot) reaction.message.author.send('Yo you got gold congrats')
                        
            })
      })
    }
  }
  client.roles.ensure(user.id, {roles: []})
  const valid = {
    "😈": "Red",
    "🔥": "Orange",
    "🍠" : "Purple",
    "✅": "Green",
    "❄" : "Blue",
    "🐷" : "Pink"
  }
  if (valid[reaction.emoji.name] && reaction.message.id === "511310076607463424") {
    if (client.roles.getProp(user.id, "roles").includes(reaction.message.guild.roles.find(r => r.name === valid[reaction.emoji.name]).id)) return
    if (client.credits.getProp(user.id, "credits") < 5000) {
      return reaction.message.channel.send("You don't have enough credits...").then(m => {
      setTimeout(() => {
        m.delete()
      }, 3000)
    })
    } else {
    user.send("Are you sure you wanna buy the " + valid[reaction.emoji.name] + " role?").then(m => {
      const filter = u => u.author === user
      m.channel.awaitMessages(filter, { max:1, time: 60000, errors: ["time"]}).then(e => {
        const resp = e.first().content
        if (resp === "yes") {
          user.send("You have purchased the role for 5000 credits!")
          client.credits.math(user.id, '-', 5000, "credits")
          client.roles.push(user.id, reaction.message.guild.roles.find(r => r.name === valid[reaction.emoji.name]).id, "roles")
        }
      })
    })
    }
  }
} 