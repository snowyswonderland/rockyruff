const getLatestGen = (gensEntries) => {
    const generationsName = [
"Red",
"Blue",
"Yellow",
"Gold",
"Silver",
"Crystal",
"Ruby",
"Sapphire",
"Emerald",
"FireRed",
"LeafGreen",
"Diamond",
"Pearl",
"Platinum",
"HeartGold",
"SoulSilver",
"Black",
"White",
"Black 2",
"White 2",
"X",
"Y",
"Omega Ruby",
"Alpha Sapphire",
"Sun",
"Moon",
  "Ultra Sun",
  "Ultra Moon"
];
  
    for (const gen of generationsName) {
    if (gensEntries[gen] !== undefined)
      return gensEntries[gen];
  }
}

module.exports = getLatestGen
